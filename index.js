var Twitter = require('twitter'),
		fs = require('fs'),
		config = require('./config.js');
var wstream = fs.createWriteStream('output.txt',{flag:'a'});
		
var client = new Twitter({
  consumer_key: config.consumer_key,
  consumer_secret: config.consumer_secret,
  access_token_key: config.access_token_key,
  access_token_secret: config.access_token_secret
});
var output = {};
client.stream('statuses/filter', {track: 'zootopia', language: 'en'}, function(stream) {
  stream.on('data', function(tweet) {
    output.id = tweet.id;
	output.name = tweet.user['name'];
	output.tweet = tweet.text;
	wstream.write(JSON.stringify(output));
	wstream.write("/n");
  });
 
  stream.on('error', function(error) {
    throw error;
  });
});
